var mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/ussd");
var Db = require("mongodb").Db,
    MongoCLient = require("mongodb").MongoClient,
    server = require("mongodb").Server;
var db = new Db("ussd", new server('localhost', '27017'));

var mongoHost = 'localhost',
        mongoPort = '27017',
        mongoCfg = 'mongodb://' + mongoHost + ':' + mongoPort + '/ussd';
        

var schema = mongoose.Schema;

var registration = new schema({
    user: {
        firstname: String,
        lastname: String,
        idno: String,
        dob: String,
        county: String,
        sex: String,
        pin: String,
        msidn: String,
    },
    counter: Number,
    sessionid: Number,
    msidn: String,
    isComplete:Boolean
})

var accessCredentials = new schema({
    token_type: String,
    access_token: String,
    refresh_token: String,
    expires_in: Number,
    id_token: String,
})

var authCredentials = new schema({
    client_id: String,
    secret_id:String
})

var register = mongoose.model("registration", registration);
 
exports.getCounter = function (msidn,clb) {
    
    db.open(function (err, connection) {
        if (connection) {
            connection.collection("registration").
                findOne({ "msidn": msidn },{"counter":1},
                function (err, data) {
                    if (err) {
                         connection.close();
                         //console.log("count", err);
                        // clb(err, null);
                    } else {
                         connection.close();
                        console.log("count-",data)
                         clb(null,data==null?0:data.counter)       
                }
                
            });
            
             
        }
       
        
    })
}

exports.createUser = function (data,clb) {  
    db.open(function (err, connection) {
        if (connection) {
            connection.collection("registration").insert(data, function (err, data) {
                if (err) {
                    console.log(err)
                } else {
                    console.log("db saved",data.result.ok)
                    connection.close();
                    clb(null, data.result.ok)

                }
            });
        }
        
    })
}

exports.addUserInfo = function (key,value, msidn, counter, clb) {
    console.log("data.result.ok")
    db.open(function (err, connection) {
        if (connection) {
        
             connection.collection("registration").update({ "msidn": msidn },
                 { $set: {[key]: value , "counter": counter } } , function (err, data) {
                if (err) {
                    console.log(err)
                } else {
                    connection.close();
                   
                    clb(null, data.result.ok)
                }
            });
        }
        
    })    
}

exports.updateCredentials = function (cred, clb) {
    db.open(function (err, connection) {
        if (connection) {
        
            connection.collection("accessCredentials").findOneAndUpdate({ "token_type": "Bearer" },
                 cred , { upsert: true }, function (err, data) {
                    if (err) {
                        console.log(err)
                      clb(data.result,null)   
                    } else {
                        connection.close();
                        
                     clb(null, data.ok)
                    }
                });
        }
    });
}
exports.getAccessCred = function (clb) {
    db.open(function (err, connection) {
        if (connection) {
            connection.collection("accessCredentials").
                findOne({ "token_type": "Bearer" },
                function (err, data) {
                    if (err) {
                        connection.close();
                        //console.log("count", err);
                        clb(err, null);
                    } else {
                        connection.close();
                       // console.log(data.access_token)
                        clb(null, data)
                    }
                
                });
            
             
        }
    })
}

exports.getAuth = function (clb) {
    db.open(function (err, connection) {
        if (connection) {
            connection.collection("auth").findOne(function (err,data) {
                if (err) {
                    clb(err, null);
                } else {
                    clb(null,data)
                }
            })
        } 
    })
}

