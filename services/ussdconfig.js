 exports.config={
    "clientcode": "55",
    "register": "1",
    "redeem": "2",
    "balance": "3",
    "basecode": "*415*55*"
}

exports.userinput = {
    "1": {
        "res": ""
    }
 }

exports.register = {
    "firstname": {
        "response": "Enter First Name",
        "counter":"1"
    } ,
    "lastname": {
        "response": " Enter Second Name",
        "counter": "2",
    },
    "idno": {
        "response": "Enter National Id Number",
        "counter": "3"
    },
    
    "dob": {
        "response": "Enter Date of Birth eg. day-month-year (01-01-1980)",
        "counter": "4"
    },"sex": {
        "response": "Enter Gender eg. Male or Female",
        "counter": "5",
    },
    "county": {
        "response": "Enter County",
        "counter": "6"
    },
    
    "pin": {
        "response": "Choose Pin/Password",
        "counter": "7"
    }
}