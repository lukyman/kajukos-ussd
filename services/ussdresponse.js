var ussdconf = require("./ussdconfig").config;
var ussdregister = require("./ussdconfig").register;
var model = require("./models");
var registration = require("./registration");
var connect = "CON ";
var end = "END ";
var httpservice = require("./httpservice");

exports.ussdresponse = function (counter, firstinput, secondinput, lastinput, msidn, sessionid, inputarray, clb) {
    console.log("firstinput", firstinput, "secondinput", secondinput)
    if (firstinput == ussdconf.clientcode && secondinput == null) {
        console.log("firstinput", firstinput, "secondinput", secondinput)
        var response = "CON Welcome to Kajukos Loyalty. Reply with" +
            "\n 1 To Register" +
            "\n 2 To Redeem" +
            "\n 3 To Check Points" +
            "\n 4 To Change Pin/Password";
            clb(null, response);
    }
    else if (firstinput == ussdconf.clientcode && secondinput == ussdconf.register) {
        console.log("here1", lastinput)
        registration.registrationResponse(counter, lastinput, msidn, inputarray, function (err, data) {

            if (err) {
                clb(err,null)
            } else {
                console.log("here", data)
                clb(null, data);
            }
        });

    } else if (firstinput == ussdconf.clientcode && secondinput == ussdconf.redeem) {
             if (counter == "1") {
             response = "CON Enter Code";
            clb(null, response);
             } else if (counter=="2"){
            response = "CON Enter Points to redeem";
                
            clb(null, response);
        } else if (counter == "3") {
            var resetpassword = {
                phone: msidn,
                code: inputarray[1],
                points: inputarray[2]
            }
            httpservice.post("api/redeem", resetpassword, function (err, data) {
                if (err) {
                    response = end + err.errorMessage;
                    clb(null, response);
                } else {
                    response = end + "Success";
                    clb(null, response);
                }
            })
        }
    } else if (firstinput == ussdconf.clientcode && secondinput == ussdconf.balance) {
        httpservice.get("api/pointbalance?phonenumber=" + msidn, function (err, data) {

            var msg = "";
            if (err) {
                msg = end + err.body.errorMessage;
                clb(msg, null)
            } else {
                console.log(data)
                msg = data==null?"END Unable to fetch balance at the moment, try again later.": end + "Your balance is " + data.model.balance;
                clb(null, msg)
            }
        })
    } else if (firstinput == ussdconf.clientcode && secondinput == "4") {
        if (counter == "1") {
            response = "CON Enter new pin";
            clb(null, response);
        } else if (counter == "2") {
            var resetpassword = {
                phoneno: msidn,
                newpassword: inputarray[1]
            }
            httpservice.post("api/clientaccount/resetpassword", resetpassword, function (err, data) {
                if (err) {
                    response = end + "Unable to change Password";
                    clb(null, response);
                } else {
                    response = end + "Password Changed Successfully";
                    clb(null, response);
                }
            })
        }
    }
    else {
        return "END Invalid Input";
    }
} 